﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

using SCC.Containers;
using SCC.Algorithms;
using SCC.Domainmodel;

namespace nunit_tests
{
    [TestFixture]
    public class algorithmTests
    {

        [Test]
        public void VelocityCalculatorInit()
        {
            var calc = new VelocityCalculator(new TrackManager());

            Assert.AreEqual(false, calc.IsInitialized(), "Calculator initialization flag behaved oddly");

            calc.Calculate(new CarState());

            Assert.AreEqual(true, calc.IsInitialized(), "Calculator did not initialize itself");

        }
        
        [Test]
        public void CalcBasicVelocity()
        {
            var calc = new VelocityCalculator(new TrackManager());

            calc.InitState(new CarState() { tick = 0, inPieceDistance = 0 });

            var state = new CarState() { tick = 1, inPieceDistance = 5.0};

            calc.Calculate(state);

            Assert.AreEqual(5.0, state.laneVelocity, "Velocity was calculated incorrectly");
        }

        [Test]
        public void CalcSuccessiveVelocities()
        {
            var calc = new VelocityCalculator(new TrackManager());

            calc.InitState(new CarState() { tick = 0, inPieceDistance = 0 });

            var state = new CarState() { tick = 1, inPieceDistance = 5.0 };

            calc.Calculate(state);

            Assert.AreEqual(5.0, state.laneVelocity, "Velocity was calculated incorrectly");

            var nextState = new CarState() { tick = 2, inPieceDistance = 7.0 };

            calc.Calculate(nextState);

            Assert.AreEqual(2.0, nextState.laneVelocity, "Velocity was calculated incorrectly");
        }
    }
}
