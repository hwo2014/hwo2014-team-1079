using SCC.Containers;
using System.Collections.Generic;
using System;
using System.Linq;

namespace SCC.Domainmodel
{
    public enum SwitchDecision
    {
        Left = -1,
        None = 0,
        Right = 1
    }

    public class domLane
    {
        public int distanceFromCenter { get; set; }
        public int index { get; set; }

        public static domLane[] CreateLanes(laneC[] laneContantainers)
        {
            domLane[] lanes = new domLane[laneContantainers.Length];

            for (int i = 0; i < laneContantainers.Length; i++)
            {
                lanes[laneContantainers[i].index] = new domLane(laneContantainers[i]);
                lanes[laneContantainers[i].index].index = laneContantainers[i].index;
            }

            return lanes;
        }

        public domLane(laneC container)
        {
            this.distanceFromCenter = container.distanceFromCenter*(-1);
            this.index = container.startLaneIndex;
        }
    }

    public class domPiece
    {
        public double? length { get; set; }
        public int? radius { get; set; }
        public bool? switchable { get; set; }
        public double? angle { get; set; }

        // index: 0->x
        public double[,] maxVelocityAtStart { get; set; }
        public int index { get; set; }
       
        public static domPiece[] CreatePieces(pieceC[] pieceContantainers, int lapCount, int laneCount)
        {
            domPiece[] pieces = new domPiece[pieceContantainers.Length];

            for (int i = 0; i < pieceContantainers.Length; i++)
            {
                pieces[i] = new domPiece(pieceContantainers[i], i);
                pieces[i].maxVelocityAtStart = new double[lapCount, laneCount];
            }

            return pieces;
        }

        public domPiece(pieceC container, int index)
        {
            this.length = container.length;
            this.radius = container.radius;
            this.switchable = container.switchable;
            this.angle = container.angle;
            this.index = index;
        }

    }

    public class domTurbo
    {
        public double turboDurationMilliseconds { get; set; }
        public double turboDurationTicks { get; set; }
        public double turboFactor { get; set; }

        public domTurbo(turboC container)
        {
            turboDurationMilliseconds = container.turboDurationMilliseconds;
            turboDurationTicks = container.turboDurationTicks;
            turboFactor = container.turboFactor;
        }
    }
    public class TrackManager
    {
        public domPiece currentPiece { get; set; }
        public domLane currentLane { get; set;  }

        public double accelerationMultiplier { get; set; }

        public Int32 currentLap { get; set;  }

        public Int32 lapCount { get; set; }

        public TrackManager()
        {
            accelerationMultiplier = 1;
        }

        public double? getPieceRadius(int pieceIndex)
        {
            return this.pieces[pieceIndex].radius;
        }

        public int HowManySwitchesBetween(int ourPieceIndex, int theirPieceIndex)
        {
            int pieceCount = this.pieces.Length;
            int switchCount = 0;
            int currentPiece = ourPieceIndex;
            for (int i = 0; i < pieceCount; i++)
            {
                if (this.pieces[currentPiece].switchable == true)
                {
                    switchCount++;
                }

                if (currentPiece == theirPieceIndex)
                {
                    break;
                }

                if (currentPiece == pieceCount - 1)
                {
                    currentPiece = 0;
                }
                else
                {
                    currentPiece++;
                }
            }

            return switchCount;
        }

        public double getPieceTotalLength(int pieceIndex, int laneIndex)
        {
            double? length = this.pieces[pieceIndex].length;
            if (length == null)
            { // no length for piece - calculate from radius&angle
                length = Math.Abs((double)this.pieces[pieceIndex].angle) * Math.PI *((double)(this.pieces[pieceIndex].radius + this.lanes[laneIndex].distanceFromCenter)) / 180.0;
            }
            return (double)length;
        }

        public void setTrackData(CarState state)
        {
            this.currentPiece = pieces[state.pieceIndex];
            this.currentLane = lanes[state.laneIndex];

            int currentPieceIndex = currentPiece.index;
            int numberOfPieces = this.pieces.Length;
            double maxVelocity = 1000000;

            this.currentLap = state.lap;
            double targetVelocity;

            //Console.WriteLine("setTrackData:[curPieceIdx:{0},currentLap:{1},numOfPieces:{2},lapCount:{3}",
            //    currentPiece.index, state.lap, this.pieces.Length, this.lapCount);
            if (currentLap == lapCount)
            {
                state.nextPieceTargetVelocity = maxVelocity;
                return;
            }

            currentLap = lapCount <= currentLap ? lapCount : currentLap;

            // last piece of last lap
            if ((currentLap == lapCount - 1 || lapCount == 0) && currentPieceIndex == numberOfPieces - 1)
            {
                targetVelocity = maxVelocity;
            }
            // last piece of lap
            else if (currentPieceIndex == numberOfPieces - 1)
            {
                //string maxVelocityAtStart = string.Join(",", this.pieces[0].maxVelocityAtStart.Select(x=> x.ToString()).ToArray());

                //Console.WriteLine("last piece, setting max vel as first piece of next lap. pieces[0].maxVelocityAtStart:{0}",
                //    maxVelocityAtStart);

                targetVelocity = this.pieces[0].maxVelocityAtStart[currentLap + 1, currentLane.index];
            }
            else if (currentLap < lapCount)
            {
                targetVelocity = this.pieces[currentPieceIndex + 1].maxVelocityAtStart[currentLap, currentLane.index];
            }
            else
                targetVelocity = maxVelocity;

            state.nextPieceTargetVelocity = targetVelocity;
        }

        public domPiece[] pieces { get; set; }
        public domLane[] lanes { get; set; }
    }
}