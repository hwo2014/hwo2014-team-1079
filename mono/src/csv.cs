using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SCC.Containers;

namespace SCC.CSV
{
    public class CSVWriter
    {
        private bool writeImmediately;

        public CSVWriter(string directory, bool writeImmediately = false)
        {
            this.dir = directory;
            this.writeImmediately = writeImmediately;

            this.stateData = new List<CarState>();
            this.crashData = new List<CrashDataC>();
        }

        public void ResetData()
        {
            this.stateData = new List<CarState>();
            this.crashData = new List<CrashDataC>();
        }

        public List<CarState> GetStateData()
        {
            return this.stateData;
        }

        public void Add(CarState pos) {
            if (this.writeImmediately)
            {
                WriteEntry(GetFile(pos), pos);
                return;
            }
            this.stateData.Add(pos);
        }

        public void Add(CrashDataC crash) {
            this.crashData.Add(crash);
        }

        public void Add(GameInitDataC init) {
            this.initData = init;
        }

        public void WriteHeaders()
        {
            string initFile = GetFile(new GameInitDataC());
            WriteHeader(initFile, new GameInitDataC());

            // pos data
            string posFile = GetFile(new CarState());
            WriteHeader(posFile, new CarState());

            // crash data
            string crashFile = GetFile(new CrashDataC());
            WriteHeader(crashFile, new CrashDataC());
        }

        public void WriteAll(bool includeHeaders = false) {
            // init data
            string initFile = GetFile(this.initData);
            WriteData(initFile, this.initData, includeHeaders);

            // pos data
            string posFile = GetFile(this.stateData.First());
            WriteData(posFile, this.stateData.Cast<Object>().ToList(), includeHeaders);

            // crash data
            string crashFile = GetFile(new CrashDataC());
            WriteData(crashFile, this.crashData.Cast<Object>().ToList(), includeHeaders);
        }

        private void WriteData(string file, List<Object> dataItems, bool includeHeaders = false) {
            if (includeHeaders)
            {
                WriteHeader(file, dataItems.First());
            }
            foreach (var item in dataItems)
            {
                WriteEntry(file, item);
            }
        }

        private void WriteData(string file, Object dataItem, bool includeHeaders = false)
        {
            if (includeHeaders)
            {
                WriteHeader(file, dataItem);
            }
            WriteEntry(file, dataItem);
        }

        private string GetFile(Object item)
        {
            var filename = "";
            if (item.GetType() == typeof(CarState))
            {
                filename =  "posData.csv";
            }
            else if (item.GetType() == typeof(GameInitDataC))
            {
                filename =  "init.csv";
            }
            else if (item.GetType() == typeof(CrashDataC))
            {
                filename = "crashData.csv";
            }
            else
            {
                filename = "other_data.csv";
            }
            return Path.Combine(this.dir, filename);
        }

        private void WriteHeader(string file, object container) {
            object toRead = container;
            string input = "";

            var props = toRead.GetType().GetProperties();

            foreach(var prop in props) {
                var val = prop.GetValue(toRead, null);
                if(string.IsNullOrEmpty(input)) {
                    input += prop.Name;
                }
                else {
                    input +=  ";" + prop.Name;
                }
            }


            using(var writer = File.AppendText(file)) {
                writer.WriteLine(input);
            }
        }

        private void WriteEntry(string file, object container) {
            object toRead = container;
            string input = "";
            var props = toRead.GetType().GetProperties();

            foreach(var prop in props) {
                var val = prop.GetValue(toRead, null);
                if(string.IsNullOrEmpty(input)) {
                    input += val.ToString();
                }
                else {
                    input +=  ";" + val.ToString();
                }
            }

            using(var writer = File.AppendText(file)) {
                writer.WriteLine(input);
            }

        }

        private string dir { get; set; }

        public enum DataType {
            init,
            position,
            crash
        }

        private List<CarState> stateData { get; set; }
        private GameInitDataC initData { get; set; }
        private List<CrashDataC> crashData { get; set; }

    }

}

