using System;
using System.Collections.Generic;

namespace SCC.Containers
{
    /// <summary>
    /// Data presentation of car state
    /// </summary>
    public class CarState
    {
        public long tick { get; set; }

        // input data
        public string name { get; set; }
        public double inPieceDistance { get; set; }
        public int pieceIndex { get; set; }
        public double pieceRadius { get; set;  }
        public double angle { get; set; }
        public int laneIndex { get; set; }
        public int lap { get; set; }

        // calculated data

        public double limitVelocity { get; set;  }
        public double laneVelocity { get; set; }

        public double laneAcceleration { get; set; }
        public double radialAcceleration { get; set; }
        public double totalAcceleration { get; set; }

        public double nextPieceTargetVelocity { get; set; }

        public double predictedVelocity { get; set; }

        // output

        public double throttle { get; set; }

        /// <summary>
        /// Reads state from raw data spec
        /// </summary>
        /// <param name="data"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static CarState Read(CarPosDataC data, int index, bool noLaps)
        {
            var state = new CarState();
            state.tick = data.gameTick;
            state.angle = data.data[index].angle;
            state.name = data.data[index].id.name;
            state.pieceIndex = data.data[index].piecePosition.pieceIndex;
            state.laneIndex = data.data[index].piecePosition.lane.endLaneIndex;
            state.inPieceDistance = data.data[index].piecePosition.inPieceDistance;
            state.lap = data.data[index].piecePosition.lap;

            if (state.lap < 0)
            {
                state.lap = 0;
            }

            if (noLaps)
                state.lap = Math.Min(state.lap, 1);

            Console.WriteLine("Read car state: tick: {0}, angle: {1}, name: {2}, pieceIdx: {3}, laneIdx: {4}, inPDist: {5}, lap: {6}",
                state.tick, state.angle, state.name, state.pieceIndex, state.laneIndex, state.inPieceDistance, state.lap);

            return state;
        }

        public static Dictionary<string, CarState> Read(CarPosDataC raw, bool noLaps) {
            Dictionary<string, CarState> states = new Dictionary<string, CarState>();
            Console.WriteLine("reading car states");
            for (int i = 0; i < raw.data.Length; i++)
            {
                var data = raw.data[i];
                states.Add(data.id.name, CarState.Read(raw, i, noLaps));
            }
            Console.WriteLine("Car states reading: done");

            return states;
        }
    }

    public class CalculationConstants
    {
        public double throttleConstant { get; set; }
        public double deaccelerationConstant { get; set; }

        public double firstDriftingAngle { get; set;  }

        public double firstDriftingVelocity { get; set; }

        public double firstDriftingRadius { get; set; }

        public double secondDriftingAngle { get; set; }

        public double secondDriftingSpeed { get; set; }

        public double maxDriftingAngle { get; set; }
    }

    public enum Direction
    {
        None,
        Left,
        Right
    }

    public class BottleNeck
    {
        public string botname { get; set; }
        public int pieceIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double avgVelDiff { get; set; }
        public int laneIndex { get; set; }
        public double distance { get; set; }
        public double curInPieceDistance { get; set; }

        /// <summary>
        /// Tick count when was last time bottleneck noticed
        /// </summary>
        public long duration { get; set; }


    }

    [Serializable]
    public class GameInitDataC
    {
        public string msgType { get; set; }
        public GameInitC data { get; set; }
    }

    [Serializable]
    public class CarPosDataC
    {
        public CarPosDataC()
        {
            this.data = new CarPosC[1];
            this.data[0] = new CarPosC();
        }
        public string msgType { get; set; }
        public CarPosC[]  data { get; set; }
        public long gameTick { get; set; }
    }

    [Serializable]
    public class CrashDataC
    {
        public string msgType { get; set; }
        public CarIdC  data { get; set; }

        public string gameId { get; set; }
        public long gameTick { get; set; }
    }

    [Serializable]
    public class CarPosC {
        public CarPosC()
        {
            this.piecePosition = new PiecePosC();
        }
        public CarIdC id { get; set; }
        public double angle { get; set; }
        public PiecePosC piecePosition { get; set; }
    }

    [Serializable]
    public class CarIdC
    {
        public string name { get; set; }
        public string color { get; set; }
    }

    [Serializable]
    public class PiecePosC
    {
        public int pieceIndex { get; set; }
        public double inPieceDistance { get; set; }
        public laneC lane { get; set; }
        public int lap { get; set; }
    }

    [Serializable]
    class LaneC
    {
        public int startLaneIndex { get; set; }
        public int endlaneIndex { get; set; }
    }

    [Serializable]
    public class GameInitC
    {
        public raceC race { get; set; }

    }

    [Serializable]
    public class carC
    {
        CarIdC id { get; set; }

        carDimC dimensions { get; set; }
    }

    [Serializable]
    public class carDimC
    {
        public double length { get; set; }
        public double width { get; set; }
        public double guideFlagPosition { get; set; }
    }

    [Serializable]
    public class raceC
    {
        public trackC track { get; set; }

        public raceSessionC raceSession { get; set;}

        public carC[] cars { get; set; }
    }

    [Serializable]
    public class trackC
    {
        public string id { get; set; }
        public string name { get; set; }
        public pieceC[] pieces { get; set; }
        public laneC[] lanes { get; set; }
        public startingpointC startingPoint { get; set; }
    }

    [Serializable]
    public class pieceC
    {
        public double? length { get; set; }
        public int? radius { get; set; }

        [Newtonsoft.Json.JsonProperty("switch")]
        public bool? switchable { get; set; }
        public double? angle { get; set; }
    }

    [Serializable]
    public class laneC
    {
        public int distanceFromCenter { get; set; }
        public int index { get; set; }
        public int startLaneIndex { get; set;  }
        public int endLaneIndex { get; set;  }
    }

    [Serializable]
    public class raceSessionC
    {
              public int laps {get; set; }
        public long maxLapTimeMs { get; set;}
        public bool quickRace { get; set ;}
    }

    [Serializable]
    public class positionC
    {
        public double x { get; set; }
        public double y { get; set; }
    }

    [Serializable]
    public class startingpointC
    {
        public positionC position { get; set; }
        public double angle { get; set; }
    }

    [Serializable]
    public class turboAvailableC
    {
        public string msgType { get; set; }
        public turboC Data { get; set; }
    }
    [Serializable]
    public class turboC
    {
        public double turboDurationMilliseconds { get; set; }
        public double turboDurationTicks { get; set; }
        public double turboFactor { get; set; }
    }

}