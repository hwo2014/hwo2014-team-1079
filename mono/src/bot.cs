using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

using SCC.Containers;
using SCC.CSV;
using SCC.Algorithms;
using SCC.Domainmodel;

public class Bot
{

    public enum BotMode
    {
        // default
        Default,
        // CSV logging for crashes
        CSV
    }

    public enum BehaviorMode
    {
        Default,

        Learning,

        AngleCrashtest
    }

    public enum Environment
    {
        Localhost,
        CI
    }

    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
        double? fixthrottle = null;

        BotMode mode = BotMode.Default;
        if (args.Length > 4 && !string.IsNullOrEmpty(args[4]))
        {
            try {
                mode = (BotMode)Enum.Parse(typeof(BotMode), args[4]);
            }
            catch(Exception) {
                Console.WriteLine("Uknown mode parameter: {0}", args[4]);
                mode = BotMode.Default;
            }
        }
        if(args.Length > 5 && !string.IsNullOrEmpty(args[5])) {

            fixthrottle = (double)Double.Parse(args[5], CultureInfo.InvariantCulture);
        }

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            try
            {
                new Bot(new BotParams() { name = botName, fixedThrottle = fixthrottle}, reader, writer, new Join(botName, botKey), mode);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(string.Format("Ex.toString(): {0}", ex.ToString()));
                Console.WriteLine(string.Format("Ex.stackTrace: {0}", ex.StackTrace));
            }
        }
    }

    private StreamWriter writer;
    private BotMode mode;
    private BehaviorMode behavior;
    private Environment env;
    private string name;
    private Dictionary<string, BottleNeck> currentBottlenecks;
    private double? fixedThrottle;

    protected class BotParams
    {
        public string name { get; set; }
        public double? fixedThrottle { get; set; }
    }

    Bot(BotParams attrs, StreamReader reader, StreamWriter writer, Join join, BotMode mode)
    {
        this.name = attrs.name;
        this.fixedThrottle = attrs.fixedThrottle;

        this.writer = writer;
        this.mode = mode;

        this.currentBottlenecks = new Dictionary<string, BottleNeck>();
        this.env = Environment.CI;
        string line;

        var logger = new CSVWriter(System.Environment.CurrentDirectory, true);

        this.behavior = BehaviorMode.Learning;

        var constants = new CalculationConstants();

        var positions = ThrottleControlCalculator.CreateThrottlePositions(40);

        var track = new TrackManager();
        var ai = new AICalculator(name, track);
        var velCalc = new VelocityCalculator(track);
        var accCalc = new AccelarationCalculator(track);
        var maxSpeedCalc = new MaxVelocityCalculator(constants);
        var control = new ThrottleControlCalculator(track, positions, constants);
        var turboCalc = new TurboCalculator(track);
        var learningCalc = new LearningCalculator();
        var driftingCalc = new DriftingCalculator();

        if (CSVLoggingEnabled())
        {
            logger.WriteHeaders();
        }

        if (this.env == Environment.CI)
        {
            send(join);
        }
        if (this.env == Environment.Localhost)
        {
            var raceJoin = new JoinRace() { botId = new BotID() { key = join.key, name = join.name }, carCount = 1, trackName = "germany" };

            send(raceJoin);
        }

        bool noLaps = false;
        bool gameInit = false;

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            Console.WriteLine("Received message: {0}", msg.msgType);
            switch (msg.msgType)
            {
                case "turboAvailable":
                    var turbo = JsonConvert.DeserializeObject<turboAvailableC>(line);
                    turboCalc.turboAvailable = true;
                    turboCalc.turbo = new domTurbo(turbo.Data);
                    turboCalc.TicksLeft = (long)turboCalc.turbo.turboDurationTicks;
                    break;
                case "carPositions":
                    if(gameInit)
                    { 
                    try
                    {
                        Console.WriteLine("Learning mode : {0}", this.behavior);

                        var posData = JsonConvert.DeserializeObject<CarPosDataC>(line);

                        var states = CarState.Read(posData, noLaps);
                        var state = states[this.name];

                        track.setTrackData(state);

                        state.pieceRadius = track.pieces[state.pieceIndex].radius.HasValue ?
                            (track.pieces[state.pieceIndex].radius.Value + track.currentLane.distanceFromCenter) : 0;

                        if (state.lap < track.lapCount)
                            state.limitVelocity = track.currentPiece.maxVelocityAtStart[state.lap, state.laneIndex];
                        velCalc.Calculate(state);
                        accCalc.Calculate(state);

                        if (this.behavior == BehaviorMode.Default)
                        {
                            if (!maxSpeedCalc.initialized)
                            {
                                //Console.WriteLine("Initialized track velocities");

                                maxSpeedCalc.CalculateMaximiumStartingVelocityForPieces(track.pieces, track.lanes,
                                    track.lanes.Length, track.lapCount);
                            }
                        }

                        //Console.WriteLine("Vel, acc calculation done");

                        if (this.behavior == BehaviorMode.Learning)
                        {
                            if (learningCalc.IsLearningFinished())
                            { // learning finished
                                this.behavior = BehaviorMode.AngleCrashtest;
                                constants = learningCalc.CalculateConstants();
                                maxSpeedCalc.consts = constants;
                                control.constants = constants;
                                driftingCalc.constants = constants;
                            }
                            else
                            {
                                learningCalc.Calculate(state);
                            }
                        }
                        else if (this.behavior == BehaviorMode.AngleCrashtest)
                        {
                            if (driftingCalc.isFinished())
                            {
                                this.behavior = BehaviorMode.Default;
                            }
                            else
                            {
                                driftingCalc.Calculate(state);
                            }
                        }
                        else
                        { // normal control
                            control.Calculate(state);
                        }

                        if (CSVLoggingEnabled())
                        {
                            logger.Add(state);
                        }

                        var direction = Direction.None;
                        if (this.behavior == BehaviorMode.Default)
                        {
                            var bottlenecks = ai.CalculateBottlenecks(states);
                            this.currentBottlenecks = ai.Merge(bottlenecks, this.currentBottlenecks);
                            direction = ai.SwitchDecision(name, state, this.currentBottlenecks);
                        }

                        //if(track.currentLane.index != 0)
                        //{
                        //    send(new Switch(SwitchDecision.Right));
                        //}

                        if (turboCalc.isTurboOn)
                        {
                            turboCalc.TicksLeft -= 1;

                            if (turboCalc.isTurboOn == false)
                                track.accelerationMultiplier = 1;
                        }

                        if (turboCalc.sendTurbo())
                        {
                            //Console.WriteLine("Sending turbo");
                            send(new Turbo());
                            turboCalc.turboAvailable = false;
                            track.accelerationMultiplier = turboCalc.turbo.turboFactor;
                        }

                        if (direction != Direction.None)
                        {
                            var dir = direction == Direction.Left ? SwitchDecision.Left : SwitchDecision.Right;
                            send(new Switch(dir, state.tick));
                            //Console.WriteLine("Switch sent");
                        }
                        else
                        {
                            if (this.fixedThrottle != null)
                            {
                                send(new Throttle((double)this.fixedThrottle, state.tick));
                            }
                            else
                            {
                                send(new Throttle(state.throttle, state.tick));
                            }
                            //Console.WriteLine("Throttle sent");
                        }

                    }
                    catch(Exception ex)
                    {
                        send(new Ping(msg.gameTick));
                        Console.WriteLine(ex.Message);
                    }
                    }  //Console.WriteLine("tick: {0}, throttle: {1}, vel: {2,2}, acc: {4,2},  maxVel: {3,2}",
                        //    state.tick, state.throttle, state.laneVelocity, state.nextPieceTargetVelocity, state.totalAcceleration);
                    break;
                case "join":
                    send(new Ping(msg.gameTick));
                    break;
                case "gameInit":

                    var initData = JsonConvert.DeserializeObject<GameInitDataC>(line);

                    track.lapCount = initData.data.race.raceSession.laps;

                    //Console.WriteLine("Track data: numOfPieces: {0}, numOfLaps: {1}",
                    //    initData.data.race.track.pieces.Length,
                    //    initData.data.race.raceSession.laps);

                    if (track.lapCount == 0)
                    {
                        noLaps = true;
                        //Console.WriteLine("Lapcount zero, setting it to 2");
                        track.lapCount = 2;
                    }
                    else
                        noLaps = false;

                    if (CSVLoggingEnabled())
                    {
                        logger.Add(initData);
                    }

                    var domPieces = SCC.Domainmodel.domPiece.CreatePieces(initData.data.race.track.pieces, track.lapCount, initData.data.race.track.lanes.Length);
                    var domLanes = SCC.Domainmodel.domLane.CreateLanes(initData.data.race.track.lanes);

                    Console.WriteLine("Created domPieces and domLanes");

                    track.pieces  = domPieces;
                    track.lanes = domLanes;

                    //SCC.ShortestPath.ShortestPath.CalculateShortestPath(domPieces, domLanes, 0);
                    gameInit = true;
                    send(new Ping(msg.gameTick));
                    break;
                case "gameEnd":
                    gameInit = false;
                    if (CSVLoggingEnabled())
                    {
                        //logger.WriteAll();
                    }
                    maxSpeedCalc.initialized = false;

                    Console.WriteLine("Race ended");
                    send(new Ping(msg.gameTick));
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
                    send(new Ping(msg.gameTick));
                    break;
                case "crash":

                    var crashData = JsonConvert.DeserializeObject<CrashDataC>(line);

                    if(!driftingCalc.isFinished() && crashData.data.name == this.name)
                    {
                        driftingCalc.currentPhase = EDriftCalculatorPhase.Crashed;
                    }

                    if (CSVLoggingEnabled())
                    {
                      
                        logger.Add(crashData);
                    }

                    send(new Ping(msg.gameTick));
                    break;
                default:
                    send(new Ping(msg.gameTick));
                    break;
            }
        }
    }

    private bool CSVLoggingEnabled()
    {
        return this.mode == BotMode.CSV;
    }

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

class MsgWrapper
{
    public string msgType;
    public Object data;
    public long gameTick;

    public MsgWrapper(string msgType, Object data, long? gameTick = null)
    {
        this.msgType = msgType;
        this.data = data;
        if (gameTick != null)
        {
            this.gameTick = (long)gameTick;
        }
    }
}

abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData(), this.gameTick));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();

    protected long? gameTick;
}

class Join : SendMsg
{
    public string name;
    public string key;
    public string color;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
        this.color = "red";
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class CreateRace : SendMsg {

    public BotID botId { get; set; }

    public string trackName { get; set; }
    public string password { get; set; }
    public int carCount { get; set; }

    public CreateRace (string track, int carCount)
	{
        this.trackName = track;
        this.carCount = carCount;


	}

    protected override string MsgType()
    {
        return "createRace";
    }
}

class JoinRace : SendMsg
{
    public BotID botId { get; set; }

    public string trackName { get; set; }
    public string password { get; set; }
    public int carCount { get; set; }


    protected override string MsgType()
    {
        return "joinRace";
    }
}

class BotID {
    public string name { get; set; }
    public string key { get; set; }
}

class Ping : SendMsg
{
    public Ping(long? gametick = null)
    {
        this.gameTick = gametick;
    }

    protected override string MsgType()
    {
        return "ping";
    }
}

class Turbo : SendMsg
{
    public string data = "Tooot tooot";

    protected override Object MsgData()
    {
        return this.data;
    }

    protected override string MsgType()
    {
        return "turbo";
    }
}
class Throttle : SendMsg
{
    public double value;

    public Throttle(double value, long? gameTick)
    {
        this.value = value;
        this.gameTick = gameTick;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}

class Switch : SendMsg
{
    public string data;

    public Switch(SwitchDecision decision, long? gameTcik)
    {
        if (decision == SwitchDecision.Left)
            data = "Left";
        else
            data = "Right";

        this.gameTick = gameTcik;
    }

    protected override Object MsgData()
    {
        return this.data;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}