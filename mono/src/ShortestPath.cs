﻿using System;
using System.Collections.Generic;
using System.Text;
using SCC.Domainmodel;

namespace SCC.ShortestPath
{
    public static class ShortestPath
    {
        public static void CalculateShortestPath(domPiece[] pieces, domLane[] lanes, int startingLaneIndex)
        {
            var PiecesWithSwitches = FindSwitches(pieces);

            int numOfSwitches = PiecesWithSwitches.Length;

            var Permutations = GetAllSwitchDecisionPermutations(numOfSwitches);

            int shortestindex = 0;
            double shortestLength = 1000000000000000;

            for (int index = 0; index < Permutations.Length; index++)
            {
                var length = CalculateTrackLength(pieces, lanes, Permutations[index], startingLaneIndex);

                shortestindex = length <= shortestLength ? index : shortestindex;
                shortestLength = length <= shortestLength ? length : shortestLength;
            }
        }

        static SwitchDecision[][] GetAllSwitchDecisionPermutations(int numberOfSwitches)
        {
            List<SwitchDecision[]> allCombinations = new List<SwitchDecision[]>();

            SwitchDecision[] current = new SwitchDecision[0];

            CalculatePossibleCombinations(numberOfSwitches, current, allCombinations);

            return allCombinations.ToArray();
        }

        static void CalculatePossibleCombinations(int nroOfSwitches, SwitchDecision[] current, List<SwitchDecision[]> allCombinations)
        {
            if (current.Length == nroOfSwitches)
            {
                allCombinations.Add(current);
                return;
            }
            SwitchDecision[] current_1 = new SwitchDecision[current.Length + 1];
            current.CopyTo(current_1,0);
            SwitchDecision[] current_2 = new SwitchDecision[current.Length + 1];
            current.CopyTo(current_2, 0);
            SwitchDecision[] current_3 = new SwitchDecision[current.Length + 1];
            current.CopyTo(current_3, 0);

            current_1[current.Length] = SwitchDecision.Left;
            current_2[current.Length] = SwitchDecision.None;
            current_3[current.Length] = SwitchDecision.Right;

            CalculatePossibleCombinations(nroOfSwitches, current_1, allCombinations);
            CalculatePossibleCombinations(nroOfSwitches, current_2, allCombinations);
            CalculatePossibleCombinations(nroOfSwitches, current_3, allCombinations);
        
        }

        static double CalculateTrackLength(domPiece[] pieces, domLane[] lanes, SwitchDecision[] switchDecisions, int startingLaneIndex)
        {
            double length = 0.0;

            int currentLaneIndex = startingLaneIndex;
            int switchIndex = 0;

            for(int index = 0; index < pieces.Length; index++)
            {
                var piece = pieces[index];

                if(piece.switchable.HasValue && piece.switchable.Value)
                {
                    var switchDecision = switchDecisions[switchIndex];

                    currentLaneIndex += (int)switchDecision;

                    if (currentLaneIndex < 0)
                        currentLaneIndex = 0;
                    else if (currentLaneIndex >= lanes.Length)
                        currentLaneIndex = lanes.Length - 1;

                    switchIndex++;

                }

                var lane = lanes[currentLaneIndex];

                length += PathLength(piece, lane);
            }

            return length;
        }

        static double PathLength(domPiece piece, domLane lane)
        {
            return piece.length.HasValue ? piece.length.Value : (piece.radius.Value + lane.distanceFromCenter) * Math.Abs(piece.angle.Value) * Math.PI / 180;
        }

        static domPiece[] FindSwitches(domPiece[] pieces)
        {
            List<domPiece> piecesWithSwitch = new List<domPiece>();

            foreach (domPiece piece in pieces)
            {
                if (piece.switchable.HasValue && piece.switchable.Value)
                    piecesWithSwitch.Add(piece);        
            }

            return piecesWithSwitch.ToArray();
        }
    }
}
