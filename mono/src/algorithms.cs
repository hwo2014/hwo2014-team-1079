using System;
using System.Collections.Generic;
using System.Linq;
using SCC.Containers;
using SCC.Domainmodel;
using SCC.CSV;

namespace SCC.Algorithms
{
    public class BaseCalculator
    {
        public BaseCalculator()
        {
            isInitializzed = false;

        }
        public bool IsInitialized() { return this.isInitializzed; }

        public void InitState(CarState state)
        {
            this.isInitializzed = true;
            this.lastState = state;
        }

        protected CarState lastState { get; set; }
        protected bool isInitializzed { get; set; }
    }

    public class VelocityCalculator : BaseCalculator
    {
        public VelocityCalculator(TrackManager track)
        {
            this.track = track;
        }

        public void Calculate(CarState state)
        {
            if (!isInitializzed)
            { // is not intialized, init & exit
                this.lastState = state;
                this.isInitializzed = true;

                state.laneVelocity = 0;
                return;
            }

            double tick_delta = (double)(state.tick - this.lastState.tick);
            if (tick_delta == 0)
            {
                // velocity cannot be calculated if no 'time' has passed
                return;
            }

            double distanceTravelled;
            if (this.lastState.pieceIndex != state.pieceIndex)
            { // piece boundary
                var restOfLastPieceDistance =
                    this.track.getPieceTotalLength(this.lastState.pieceIndex, this.lastState.laneIndex) - this.lastState.inPieceDistance;
                distanceTravelled = restOfLastPieceDistance + state.inPieceDistance;
            }
            else
            { // normal case
                distanceTravelled = state.inPieceDistance - this.lastState.inPieceDistance;
            }

            var vel = distanceTravelled / tick_delta;

            state.laneVelocity = vel;

            // update lastState
            this.lastState = state;
        }


        private TrackManager track { get; set; }
    }

    public class AccelarationCalculator : BaseCalculator
    {
        public AccelarationCalculator(TrackManager track)
        {
            this.track = track;

        }

        public void Calculate(CarState state)
        {
            if (!isInitializzed)
            { // is not intialized, init & exit
                this.lastState = state;
                this.isInitializzed = true;

                state.laneVelocity = 0;
                return;
            }

            double tick_delta = (double)(state.tick - this.lastState.tick);
            if (tick_delta == 0)
            {
                // acc cannot be calculated if no 'time' has passed
                return;
            }

            // lane acceleration
            var acc = state.laneVelocity - this.lastState.laneVelocity;
            state.laneAcceleration = acc;

            // radial acceleration
            double acc_r;
            var radius = track.getPieceRadius(state.pieceIndex);
            if (radius != null)
            {
                acc_r = Math.Pow(state.laneVelocity, 2.0) / (track.currentPiece.radius.Value + track.currentLane.distanceFromCenter);
            }
            else
            { // no radius -> radial acceleration = 0
                acc_r = 0.0;
            }
            state.radialAcceleration = acc_r;

            // total acceleration
            double acc_total;
            acc_total = Math.Sqrt(Math.Pow(state.radialAcceleration, 2.0) + Math.Pow(state.laneAcceleration, 2.0));
            state.totalAcceleration = acc_total;

            // update lastState
            this.lastState = state;

        }

        private TrackManager track;
    }


    public class ThrottleControlCalculator : BaseCalculator
    {
        public ThrottleControlCalculator(TrackManager track,
            List<double> throttlePositions, CalculationConstants constants)
        {
            this.track = track;
            this.throttlePositions = throttlePositions;
            this.constants = constants;

        }

        public static List<double> CreateThrottlePositions(int numberOfPositions)
        {
            List<double> positions = new List<double>();
            var step = 1.0 / numberOfPositions;
            var currentPos = step;
            for (int i = 0; i < numberOfPositions; i++)
            {
                positions.Add(Math.Round(currentPos, 3, MidpointRounding.AwayFromZero));
                currentPos += step;
                
            }
            return positions;
        }

        public double CalculateOptimalThrottle(CarState state)
        {
            var limitVelocity = CalculateLimitVelocity(state);
            var aMax = limitVelocity - state.laneVelocity;
            return (aMax - constants.deaccelerationConstant * state.laneVelocity)/(constants.throttleConstant * track.accelerationMultiplier);
        }

        public double CalculateZeroAccThrottle(CarState state)
        {
            return - constants.deaccelerationConstant * state.laneVelocity/ constants.throttleConstant;
        }

        public void Calculate(CarState state)
        {
            foreach (var throttle in throttlePositions.OrderByDescending(x => x))
            { // iterate possible throttle positions
                var nextState = CalculateNextState(state, throttle);

                // if limit velocity is not exceeded
                if (checkIfIsValidControll(state, nextState))
                { // if next state can be driven to target
                    // set throttle and return
                    state.throttle = throttle;

                    state.predictedVelocity = state.laneVelocity + nextState.laneAcceleration;

                    return;
                }
            }

            var nState = CalculateNextState(state, 0);

            state.throttle = 0;

            state.predictedVelocity = state.laneVelocity + nState.laneAcceleration;

        }

        private bool checkIfIsValidControll(CarState currentState, CarState nextState)
        {
            var limitVelocity = CalculateLimitVelocity(currentState);

            var currentLimitVelocity = currentState.limitVelocity - 0.1;

            double totalAcceleration = nextState.totalAcceleration;

            bool isValid = nextState.laneVelocity < limitVelocity;

            if(currentState.pieceRadius != 0)
            {
                isValid &= nextState.laneVelocity < currentLimitVelocity;
            }

            return isValid;
        }


        /// <summary>
        /// Calculates next state with given data
        /// </summary>
        /// <param name="state"></param>
        /// <param name="throttle"></param>
        /// <returns></returns>
        private CarState CalculateNextState(CarState state, double throttle)
        {
            var piece = this.track.currentPiece;
            var lane = this.track.currentLane;

            var aThrottle = constants.throttleConstant * throttle * track.accelerationMultiplier;
            var aLane = aThrottle  + constants.deaccelerationConstant * state.laneVelocity;

            var next = new CarState();

            next.laneVelocity = state.laneVelocity + aLane;
            next.laneAcceleration = aLane;

            next.radialAcceleration = piece.angle.HasValue ? next.laneVelocity * next.laneVelocity / (piece.radius.Value + lane.distanceFromCenter) : 0;

            next.totalAcceleration = Math.Sqrt(next.radialAcceleration * next.radialAcceleration + next.laneAcceleration *next.laneAcceleration);
            return next;
        }

        private double CalculateLimitVelocity(CarState state)
        {
            double distanceLeft = track.getPieceTotalLength(state.pieceIndex, state.laneIndex) - state.inPieceDistance;
            var currentStateLimitVelocity = state.nextPieceTargetVelocity -  this.constants.deaccelerationConstant * distanceLeft;

            return currentStateLimitVelocity;
        }

        private List<double> throttlePositions { get; set; }


        public CalculationConstants constants { get; set; }

        private TrackManager track { get; set; }
    }

    public class TurboCalculator
    {
        public TrackManager track { get; set; }

        public bool turboAvailable { get; set; }
        
        public domTurbo turbo { get; set; }

        public bool isTurboOn { get; set; }

        private long _ticksLeft { get; set; }

        public long TicksLeft
        {
            get { return _ticksLeft; }
            set
            {
                if (value <= 0)
                    isTurboOn = false;
                _ticksLeft = 0;
            }
        }

        public TurboCalculator(TrackManager track)
        {
            this.track = track;
        }

        public bool sendTurbo()
        {
            if (!turboAvailable)
                return false;

            if(this.track.pieces.Where(x => x.index >= this.track.currentPiece.index).All(y => y.angle.HasValue == false))
            {
                return true;
            }
            else
            {
                return false;
            }
           
        }
    }

    public class MaxVelocityCalculator
    {

        public CalculationConstants consts { get; set;  }

        public bool initialized { get; set;  }

        public MaxVelocityCalculator(CalculationConstants consts)
        {
            this.consts = consts;
        }

        public void CalculateMaximiumStartingVelocityForPieces(domPiece[] pieces, domLane[] lanes, int laneCount, int laps)
        {
            double min = 10000;
            double currentSlipAngle = 0;
            for (int lane = 0; lane < laneCount; lane++)
            {
                for (int lap = laps - 1; lap >= 0; lap--)
                {
                    for (int index = pieces.Length - 1; index >= 0; index--)
                    {
                        Console.WriteLine("Calculating max velocity for piece: {0}, lane: {1}, lap: {2}", index, lane, lap);
                        var piece = pieces[index];

                        double MaxVelocity;

                        int similarCount = 0;

                        double distance = 0;

                        if (piece.angle.HasValue)
                        {
                            double angleMultiplier = piece.angle.Value / Math.Abs(piece.angle.Value);

                            Console.WriteLine("anglemultiplier {0}",angleMultiplier);

                            double MaxSlipAngle = consts.maxDriftingAngle - 5;

                            double B = 180 / Math.PI * (consts.firstDriftingVelocity / consts.firstDriftingRadius);
                            double A = consts.firstDriftingAngle;

                            Console.WriteLine("frist driftin vel {0}, first drifting rad{1}", consts.firstDriftingVelocity, consts.firstDriftingRadius);
                            double BSlip = B - A;

                            double radius = 0;

                            similarCount = getSimilarPieces(index, pieces, lanes[lane], out radius);

                            double VThreshold = Math.PI / 180 * BSlip * (consts.firstDriftingRadius);

                            double Fflip = VThreshold * VThreshold / ((consts.firstDriftingRadius));

                            double SlipVelocity = Math.Sqrt(Fflip * (radius));

                            Console.WriteLine("Radius {0}", radius);
                            double AnglesFirstsTimeDerivate = (consts.secondDriftingAngle - consts.firstDriftingAngle);

                            //distance = distance != 0 ? distance : (similarCount + 1) * Math.PI / 180 * Math.Abs(piece.angle.Value) * (piece.radius.Value + lanes[lane].distanceFromCenter);

                            MaxVelocity = Math.Abs(distance * SlipVelocity * AnglesFirstsTimeDerivate / (AnglesFirstsTimeDerivate * distance - (MaxSlipAngle - angleMultiplier * currentSlipAngle)));

                            if (MaxVelocity <= SlipVelocity)
                                MaxVelocity = SlipVelocity;

                            currentSlipAngle += angleMultiplier * distance
                                 * AnglesFirstsTimeDerivate * (MaxVelocity - SlipVelocity) / (MaxVelocity);

                            MaxVelocity = SlipVelocity;
                        }
                        else
                        {
                            currentSlipAngle = 0;
                            MaxVelocity = 1000000;
                            similarCount = 0;
                        }

                        if ((index == pieces.Length - 1) && (lap == laps - 1))
                        {
                            piece.maxVelocityAtStart[lap, lane] = MaxVelocity;
                        }
                        else
                        {
                            for (int i = 0; i < similarCount + 1; i++)
                            {

                                double nextMaxVelocity = index == pieces.Length - 1 ? pieces[0].maxVelocityAtStart[lap + 1, lane] : pieces[index + 1].maxVelocityAtStart[lap, lane];

                                distance = piece.angle.HasValue ? (1) * Math.PI / 180 * Math.Abs(piece.angle.Value) * (piece.radius.Value + lanes[lane].distanceFromCenter) : piece.length.Value;

                                double maxSpeedToAvoidNextPiecesMaxSpeed = nextMaxVelocity - consts.deaccelerationConstant * distance;

                                piece.maxVelocityAtStart[lap, lane] = Math.Min(maxSpeedToAvoidNextPiecesMaxSpeed, MaxVelocity);

                                Console.WriteLine("maxvelocity {0} lane {1} lap {2}", MaxVelocity, lane, lap);
                                if (index == 0)
                                    continue;

                                index--;
                                piece = pieces[index];
                                piece.maxVelocityAtStart[lap, lane] = Math.Min(maxSpeedToAvoidNextPiecesMaxSpeed, MaxVelocity);
                            }


                            if (piece.maxVelocityAtStart[lap, lane] < 0)
                            {
                                int breakhere;
                            }

                            if (piece.maxVelocityAtStart[lap, lane] < min)
                                min = piece.maxVelocityAtStart[lap, lane];
                        }

                        distance = 0;
                        Console.WriteLine("Max vel calculated. Vel: {0}", piece.maxVelocityAtStart[lap, lane]);
                    }
                }

                this.initialized = true;
            }
        }

       
        public int getSimilarPieces(int index, domPiece[] pieces, domLane lane, out double radius)
        {
            var cpiece = pieces[index];

            radius = cpiece.radius.Value;

            index--;
            int similarCount = 0;

            while(index >=0)
            {
                if (pieces[index].angle.HasValue)
                {
                    radius = radius < pieces[index].radius.Value ? radius : pieces[index].radius.Value;
                    similarCount++;
                }
                else
                    break;

                index--;
            }

            radius += lane.distanceFromCenter;

            return similarCount;
        }
    }

    public enum EDriftCalculatorPhase
    {
        WaitingForAngle,
        WaitingForSecondAngle,
        DrivingFullSpeed,
        Crashed,
        Finished
    }

    public class DriftingCalculator
    {


        public CalculationConstants constants { get; set;  }

        double currentMaxAngle;

        public EDriftCalculatorPhase currentPhase  { get; set;}
    
        public bool isFinished()
        {
            return this.currentPhase == EDriftCalculatorPhase.Finished;
        }

        public DriftingCalculator()
        {
            this.currentPhase = EDriftCalculatorPhase.WaitingForAngle;
        }

        public void Calculate(CarState state)
        {
            state.throttle = 1;

            if(currentPhase == EDriftCalculatorPhase.WaitingForAngle && state.angle > 0 && state.pieceRadius > 0 && state.laneVelocity > 0)
            {
                Console.WriteLine("first drift angle{0}, rad {1}", state.angle, state.pieceRadius);
                constants.firstDriftingAngle = state.angle;
                constants.firstDriftingVelocity= state.laneVelocity;
                constants.firstDriftingRadius = state.pieceRadius;

                currentPhase = EDriftCalculatorPhase.WaitingForSecondAngle;
            }
            else if (currentPhase == EDriftCalculatorPhase.WaitingForSecondAngle)
            {
                constants.secondDriftingAngle = state.angle;
                constants.secondDriftingSpeed = state.laneVelocity;

                currentPhase = EDriftCalculatorPhase.DrivingFullSpeed;
            }  
            if(currentPhase == EDriftCalculatorPhase.Crashed)
            {
                constants.maxDriftingAngle = currentMaxAngle;
                this.currentPhase = EDriftCalculatorPhase.Finished;
            }

            currentMaxAngle = state.angle;
        }
    }

    public class LearningCalculator
    {
        public LearningCalculator()
        {
            this.constants = new CalculationConstants();
            this.currentPhase = ELearningPhase.Wait;
            this.ticksUsedInPhase = 0;
            this.learningPhases = new Dictionary<ELearningPhase, LearningConfig>() {
                { ELearningPhase.Wait, new LearningConfig() { throttleToUse = 0, ticksToUse = 5}}, // init with max throttle of 10 ticks before starting tests
                { ELearningPhase.Acceleration, new LearningConfig() { throttleToUse = 1.0, ticksToUse = 1 }},
                { ELearningPhase.Deacceleration, new LearningConfig() { throttleToUse = 0, ticksToUse = 1}}, // acc with throttle 1.0
                { ELearningPhase.DeaccelerationEnding, new LearningConfig() { throttleToUse = 0.0, ticksToUse = 1 }} // deacc with throttle 0
            };
            this.phaseData = new Dictionary<ELearningPhase, List<CarState>>();

            this.logger = new CSVWriter("");
        }

        public void Calculate(CarState state)
        {
            this.ticksUsedInPhase++;

            AddControl(state);
            logger.Add(state);

            if (this.ticksUsedInPhase == this.learningPhases[this.currentPhase].ticksToUse)
            { // switch phase
                this.ticksUsedInPhase = 0;

                this.phaseData[this.currentPhase] = logger.GetStateData();

                logger.ResetData();
                this.currentPhase = NextState(this.currentPhase);
            }
        }

        /// <summary>
        /// Returns boolean indicating if learning has finished.
        /// </summary>
        /// <returns></returns>
        public bool IsLearningFinished()
        {
            return this.currentPhase == ELearningPhase.Finished;
        }

        public CalculationConstants CalculateConstants()
        {
            this.constants.throttleConstant = this.phaseData[ELearningPhase.Deacceleration].FirstOrDefault().totalAcceleration;

            this.constants.deaccelerationConstant = this.phaseData[ELearningPhase.DeaccelerationEnding].FirstOrDefault().laneAcceleration / this.phaseData[ELearningPhase.Deacceleration].FirstOrDefault().laneVelocity;

            return this.constants;
        }

        private ELearningPhase NextState(ELearningPhase state)
        {
            if (state == ELearningPhase.Wait) return ELearningPhase.Acceleration;
            if (state == ELearningPhase.Acceleration) return ELearningPhase.Deacceleration;
            if (state == ELearningPhase.Deacceleration) return ELearningPhase.DeaccelerationEnding;
            return ELearningPhase.Finished;
        }

        private void AddControl(CarState state)
        {
            state.throttle = this.learningPhases[this.currentPhase].throttleToUse;
        }

        public CalculationConstants constants { get; private set; }
        // current learning state
        private ELearningPhase currentPhase { get; set; }
        // ticks used in current learning state
        private int ticksUsedInPhase { get; set; }

        // how many ticks to use per learning state
        private Dictionary<ELearningPhase, LearningConfig> learningPhases;
        // logged data during learning
        private Dictionary<ELearningPhase, List<CarState>> phaseData;
        private CSVWriter logger;

        protected enum ELearningPhase
        {
            Wait,
            Acceleration,
            Deacceleration,
            DeaccelerationEnding,
            Finished
        }

        protected class LearningConfig
        {
            public int ticksToUse { get; set; }
            public double throttleToUse { get; set; }

        }
    }

    public class AICalculator
    {
        private TrackManager track;
        private double lowerVelLimitFactor = 0.8;
        private string botName;

        public AICalculator(string botname, TrackManager track)
        {
            this.track = track;
            this.botName = botname;
        }

        public Dictionary<string, BottleNeck> CalculateBottlenecks(Dictionary<string, CarState> states)
        {
            Dictionary<string, BottleNeck> bottlenecks = new Dictionary<string, BottleNeck>();

            foreach (var state in states)
            {
                Console.WriteLine("bottleneck calc, considering car: {0}", state.Key);
                Console.WriteLine("state.val.pieceIdx: {0}, state.val.lap: {1}, state.val.laneIdx: {2}",
                    state.Value.pieceIndex, state.Value.lap, state.Value.laneIndex);
                Console.WriteLine("maxVelAtStart: {0}", track.pieces[state.Value.pieceIndex]);
                Console.WriteLine("maxVelAtStart: {0}", track.pieces[state.Value.pieceIndex].maxVelocityAtStart[state.Value.lap, state.Value.laneIndex]);
                if (state.Value.laneVelocity < this.lowerVelLimitFactor * track.pieces[state.Value.pieceIndex].maxVelocityAtStart[state.Value.lap, state.Value.laneIndex])
                {
                    bottlenecks.Add(state.Key, new BottleNeck()
                    {
                        avgVelDiff = state.Value.laneVelocity / track.pieces[state.Value.pieceIndex].maxVelocityAtStart[state.Value.lap, state.Value.laneIndex],
                        botname = state.Key,
                        duration = 1,
                        laneIndex = state.Value.laneIndex,
                        pieceIndex = state.Value.pieceIndex,
                        curInPieceDistance = state.Value.inPieceDistance
                    });
                }
            }
            return bottlenecks;
        }

        public Dictionary<string, BottleNeck> Merge(Dictionary<string, BottleNeck> current, Dictionary<string, BottleNeck> old)
        {
            foreach (var bottleneck in old)
            {
                if (current.ContainsKey(bottleneck.Key))
                {
                    current[bottleneck.Key].duration += bottleneck.Value.duration;
                    current[bottleneck.Key].avgVelDiff += bottleneck.Value.avgVelDiff;
                    current[bottleneck.Key].avgVelDiff /= 2.0;
                }
                else
                {
                    current.Add(bottleneck.Key, bottleneck.Value);
                }

            }
            return current;
        }

        public Direction SwitchDecision(string ourName, CarState us, Dictionary<string, BottleNeck> bottlenecks)
        {
            int minDuration = 20; // min ticks to consider bottleneck to cause harm
            bottlenecks.Remove(ourName);

            if(track.pieces[us.pieceIndex].switchable != true) {
                // piece does not have switch
                return Direction.None;
            }

            var currentLaneBlocked = bottlenecks
                .Where(x => x.Value.laneIndex == us.laneIndex &&
                    x.Value.duration > minDuration && 
                    this.track.HowManySwitchesBetween(us.pieceIndex, x.Value.pieceIndex) == 1);
            // filter those behind us in current block
            currentLaneBlocked = currentLaneBlocked.Where(x => (x.Value.pieceIndex != us.pieceIndex)
                || (x.Value.curInPieceDistance > us.inPieceDistance)).OrderBy(x => x.Value.avgVelDiff);

            if (currentLaneBlocked.Count() > 0)
            { // consider switching
                // consider to switching inner
                if (track.lanes.Count() > us.laneIndex + 1)
                {
                    var blocks = bottlenecks.Where(x => x.Value.laneIndex == us.laneIndex + 1
                        && x.Value.duration > minDuration &&
                        this.track.HowManySwitchesBetween(us.pieceIndex, x.Value.pieceIndex) == 1);
                    // inner lane is also blocked
                    if (blocks.Count() > 0)
                    {
                        var slowest = blocks.OrderBy(x => x.Value.avgVelDiff).First();
                        if (slowest.Value.avgVelDiff < currentLaneBlocked.First().Value.avgVelDiff)
                        { // inner lane is slower
                            return Direction.None;
                        }
                        else
                        { // inner lane is faster
                            return Direction.Right;
                        }
                    }
                    // inner lane is free, switch to it
                    else
                    {
                        return Direction.Right;
                    }
                }
                // consider to switch outer
                else if (us.laneIndex > 0)
                {
                    var blocks = bottlenecks.Where(x => x.Value.laneIndex == us.laneIndex - 1
                        && x.Value.duration > minDuration &&
                        this.track.HowManySwitchesBetween(us.pieceIndex, x.Value.pieceIndex) == 1);
                    // if outer lane is also blocked
                    if (blocks.Count() > 0)
                    {
                        var slowest = blocks.OrderBy(x => x.Value.avgVelDiff).First();

                        if (slowest.Value.avgVelDiff < currentLaneBlocked.First().Value.avgVelDiff)
                        { // outer lane is slower
                            return Direction.None;
                        }
                        else
                        { // outer lane is faster
                            return Direction.Left;
                        }

                    }
                    // outer lane is free, switch to it
                    else
                    {
                        return Direction.Left;
                    }
                }
            }

            // if switch 
            return Direction.Right;
        }
    }
}